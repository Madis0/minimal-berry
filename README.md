# Minimal Berry

Minimal Berry is a familiar, yet minimal configuration preset for the Android app [Berry Browser](https://play.google.com/store/apps/details?id=jp.ejimax.berrybrowser). 

While possible to use on any screen size, this configuration is mainly targeted for modern smartphones in portrait mode. For big screens, like tablets and foldables, I recommend [Vivaldi](https://play.google.com/store/apps/details?id=com.vivaldi.browser).

## Benefits

- Familiar - Minimal Berry is inspired by Google Chrome's look and feel
- Feature-rich - Berry Browser has an ad blocker, dark mode, website translation, userscript support and more 
- Smart - thin persistent address bar, auto-hiding tab bar and intuitive gestures create a comfortable experience
- Private - DuckDuckGo is the default search engine, HTTPS is preferred by default and other tweaks
- Secure - Berry Browser uses the built-in [Google's WebView](https://play.google.com/store/apps/details?id=com.google.android.webview), which protects from exploits and malware sites
- Customizable - everything you see here can be further tweaked by you in the app's settings
- Lightweight - this backup does not contain or overwrite your history, bookmarks and other non-UI contents inside Berry Browser

## Installation

1. Get [Berry Browser](https://play.google.com/store/apps/details?id=jp.ejimax.berrybrowser)
2. [Download latest Minimal Berry berrybackup](../../releases)
3. Run Berry Browser
4. Navigate `⋮` -> `Settings` -> `Import/Export` -> `Restore settings` -> `Minimal_Berry_vX.berrybackup`
5. Enjoy! 

## Screenshots

<img src="../../raw/main/.gitlab/Screenshots/NewTab.png" height="750" alt="New tab">
<img src="../../raw/main/.gitlab/Screenshots/Site.png" height="750" alt="Site">
<img src="../../raw/main/.gitlab/Screenshots/Menu.png" height="750" alt="Menu">
<img src="../../raw/main/.gitlab/Screenshots/Landscape.png"  alt="Landscape" width="750">
